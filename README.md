# How to install the application

1. Clone the repository `git clone https://qonand@bitbucket.org/qonand/homework4.git`
2. Run `docker-compose up -d nginx mysql workspace` in `laradock` folder
3. Run `docker-compose exec workspace bash` in `laradock` folder and in opened bash run the following commands:

 - `composer install`
 - `php artisan key:generate`
 - `php artisan migrate`

# How to run the application
1. Run `docker-compose up -d nginx mysql workspace` in `laradock` folder
2. Run Siege to start stress testing in the project folder, for example `siege -d1 -c25 -t15s --file=urls.txt`

# Result
Result of probabilistic cache flushing implementation you can see here https://bitbucket.org/qonand/homework4/src/master/app/app/Components/ProbabilisticCache.php

Result of stress testing with Siage:
![Scheme](result.png)
