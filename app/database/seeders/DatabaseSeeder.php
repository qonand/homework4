<?php

namespace Database\Seeders;

use App\Models\Client;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * @var int
     */
    private const CLIENTS_AMOUNT = 1000;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Client::factory(static::CLIENTS_AMOUNT)->create();
    }
}
