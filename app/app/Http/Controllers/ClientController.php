<?php

namespace App\Http\Controllers;

use App\Components\ProbabilisticCache;
use App\Models\Client;
use Illuminate\Http\Request;
use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;

class ClientController extends Controller
{
    /**
     * @var string
     */
    const CACHE_KEY = 'random-client';

    /**
     * @var int
     */
    const CACHE_TTL = 300;

    /**
     * @var ProbabilisticCache
     */
    private $cache;

    public function __construct()
    {
        $cache = app(CacheInterface::class);
        $this->cache = new ProbabilisticCache($cache);
    }

    /**
     * @param Request $request
     *
     * @return Client
     */
    public function create(Request $request): Client
    {
        $model = Client::create($request->all());

        return $model;
    }

    /**
     * @return Client
     * @throws InvalidArgumentException
     */
    public function getRandom(): ?Client
    {
        return $this->cache->getOrSet(static::CACHE_KEY, static::CACHE_TTL, static function (){
            $min = Client::min('id');
            $max = Client::max('id');

            if (!$min || !$max) {
                return null;
            }

            $id = random_int($min, $max);
            return Client::firstWhere('id', $id);
        });
    }
}
