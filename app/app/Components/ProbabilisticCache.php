<?php

namespace App\Components;

use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;

class ProbabilisticCache
{
    /**
     * @var CacheInterface
     */
    private $originalCache;

    /**
     * @param CacheInterface $originalCache
     */
    public function __construct(CacheInterface $originalCache)
    {
        $this->originalCache = $originalCache;
    }

    /**
     * @param string $key
     * @param int $ttl
     * @param callable $function
     * @return mixed|void
     * @throws InvalidArgumentException
     */
    public function getOrSet(string $key, int $ttl, callable $function)
    {
        $data = $this->originalCache->get($key);
        ['value' => $value, 'delta' => $delta, 'expired' => $expired] = $data;

        $hasCorrectData = $delta !== null && $expired !== null;

        $isNeedRefresh =  !$hasCorrectData || time() - $delta * log(rand(0,1)) >= $expired;
        if (!$isNeedRefresh) {
            return $value;
        }

        $start = time();
        $expired = $start + $ttl;
        $value = call_user_func($function);
        $delta = time() - $start;

        $this->originalCache->set($key, [
            'value' => $value,
            'delta' => $delta,
            'expired' => $expired,
        ]);
    }
}
